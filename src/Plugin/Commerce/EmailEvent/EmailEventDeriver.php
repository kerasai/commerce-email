<?php

namespace Drupal\commerce_email\Plugin\Commerce\EmailEvent;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Deriver for entity events.
 */
class EmailEventDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $entity_type = $base_plugin_definition['entity_type'];

    foreach (['insert', 'update', 'delete'] as $op) {
      $label_op = ($op == 'insert') ? 'add' : $op;
      $id = "commerce_email_entity_{$entity_type}_$op";
      $this->derivatives[$id] = [
        'id' => $id,
        'label' => ucfirst("$entity_type $label_op"),
        'event_name' => "commerce_email.entity_{$entity_type}_$op",
      ] + $base_plugin_definition;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
