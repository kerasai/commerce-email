<?php

namespace Drupal\commerce_email\Plugin\Commerce\EmailEvent;

use Drupal\commerce_email\Event\EntityEvent;
use Drupal\Component\EventDispatcher\Event;

/**
 * Abstract class for implementing emails for entities.
 *
 * Extend this implement to a plugin using only an annotation. Also, utilize the
 * \Drupal\commerce_email\Plugin\Commerce\EmailEvent\EmailEventDeriver deriver
 * to automatically implement insert, update, and delete events.
 *
 * Example:
 *
 *   id = "my_module_some_entity_type",
 *   entity_type = "some_entity_type",
 *   deriver = "\Drupal\commerce_email\Plugin\Commerce\EmailEvent\EmailEventDeriver"
 */
abstract class EntityEmailEventBase extends EmailEventBase {

  /**
   * {@inheritdoc}
   */
  public function extractEntityFromEvent(Event $event) {
    assert($event instanceof EntityEvent);
    return $event->getEntity();
  }

}
